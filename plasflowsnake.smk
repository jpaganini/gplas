configfile: "templates/final.yaml"

rule awk_links:
    input:
        lambda wildcards: config["samples"][wildcards.sample]
    output:
        "gplas_input/{sample}_raw_links.txt"
    conda:
        "envs/r_packages.yaml"
    message:
        "Extracting the links from the graph {input}"
    log:
        "logs/{sample}_log_links.txt"
    shell:
        """awk -F "\\t" '{{if($1 == "L") print $N}}' {input}  1>> {output} 2>> {log}"""

rule awk_nodes:
    input:
        lambda wildcards: config["samples"][wildcards.sample]
    output:
        "gplas_input/{sample}_raw_nodes.fasta"
    conda:
        "envs/r_packages.yaml"
    message:
        "Extracting the nodes from the graph {input}"
    log:
        "logs/{sample}_log_nodes.txt"
    shell:
        """awk '{{if($1 == "S") print ">"$1$2"_"$4"_"$5"\\n"$3}}' {input}  1>> {output} 2>> {log}"""

rule plasflow:
    input:
        "gplas_input/{sample}_raw_nodes.fasta"
    output:
        "plasflow_prediction/{sample}_plasmid_prediction.tab"
    params:
        species = config["species"],
        prob = config["threshold_prediction"],
        threshold = config["threshold_prediction"]
    conda:
        "envs/plasflow.yaml"
    message:
        "Running plasflow to obtain the plasmid prediction using the nodes extracted from the graph. If this is the first time running plasflow, installation can take a few minutes"
    shell:
        """current_path=$(pwd) && cd ~ && PlasFlow.py --input "$current_path"/{input} --threshold {params.threshold} --output "$current_path"/{output}"""

rule gplas_coverage:
    input:
        nodes="gplas_input/{sample}_raw_nodes.fasta",
	    links="gplas_input/{sample}_raw_links.txt",
        prediction="plasflow_prediction/{sample}_plasmid_prediction.tab"
    output:
        coverage="coverage/{sample}_estimation.txt",
        graph_contigs="coverage/{sample}_graph_contigs.tab",
    	graph_repeats="coverage/{sample}_repeats_graph.tab",
    	clean_links="coverage/{sample}_clean_links.tab",
    	clean_prediction="coverage/{sample}_clean_prediction.tab",
    	initialize_nodes="coverage/{sample}_initialize_nodes.tab"
    params:
        classifier = config["classifier"],
        threshold = config["threshold_prediction"]
    conda:
        "envs/r_packages.yaml"
    message:
        "Extracting the sd k-mer coverage from the chromosome-predicted contigs"
    script:
        "scripts/gplas_coverage.R"

rule gplas_paths:
    input:
        nodes="gplas_input/{sample}_raw_nodes.fasta",
	    clean_links="coverage/{sample}_clean_links.tab",
        prediction="plasflow_prediction/{sample}_plasmid_prediction.tab",
        coverage="coverage/{sample}_estimation.txt",
        graph_contigs="coverage/{sample}_graph_contigs.tab",
        graph_repeats="coverage/{sample}_repeats_graph.tab",
        clean_prediction="coverage/{sample}_clean_prediction.tab",
        initialize_nodes="coverage/{sample}_initialize_nodes.tab"
    output:
        solutions="walks/{sample}_solutions.csv",
        connections="walks/{sample}_connections.tab"
    params:
        iterations = config["number_iterations"],
        classifier = config["classifier"],
        filt_gplas = config["filt_gplas"]
    conda:
        "envs/r_packages.yaml"
    message:
        "Searching for plasmid-like walks using a greedy approach"
    threads: 1
    script:
        "scripts/gplas_paths.R"

rule gplas_coocurr:
    input:
        nodes="gplas_input/{sample}_raw_nodes.fasta",
	    clean_links="coverage/{sample}_clean_links.tab",
        prediction="plasflow_prediction/{sample}_plasmid_prediction.tab",
        coverage="coverage/{sample}_estimation.txt",
        graph_contigs="coverage/{sample}_graph_contigs.tab",
        graph_repeats="coverage/{sample}_repeats_graph.tab",
        clean_prediction="coverage/{sample}_clean_prediction.tab",
        initialize_nodes="coverage/{sample}_initialize_nodes.tab",
        solutions="walks/{sample}_solutions.csv"
    output:
        plot_graph="results/{sample}_plasmidome_network.png",
        components="results/{sample}_bins.tab",
        results="results/{sample}_results.tab"
    params:
        threshold = config["threshold_prediction"],
        classifier = config["classifier"],
        iterations = config["number_iterations"],
        edge_gplas = config["edge_gplas"],
        sample = config["name"],
        modularity_threshold = config["modularity_threshold"]
    conda:
        "envs/r_packages.yaml"
    message:
        "Generating weights for the set of new edges connecting plasmid unitigs"
    script:
        "scripts/gplas_coocurrence.R"


rule quast_alignment:
    input:
        nodes="gplas_input/{sample}_raw_nodes.fasta",
        reference="reference_genome/{sample}_ref_genome.fasta"
    output:
        align=directory("evaluation/{sample}_alignments")
    conda:
        "envs/quast.yaml"
    shell:
        "quast.py -R {input.reference} --silent -a all -m 1000 -o {output.align} {input.nodes}"

rule awk_parsing_alignment:
    input:
        alignment=directory("evaluation/{sample}_alignments")
    output:
        "evaluation/{sample}_alignment_test.txt"
    shell:
        """awk '{{print $5,$6}}' {input.alignment}/contigs_reports/*_raw_nodes.tsv > {output}"""

rule gplas_evaluation:
    input:
        nodes="gplas_input/{sample}_raw_nodes.fasta",
	    clean_links="coverage/{sample}_clean_links.tab",
        prediction="plasflow_prediction/{sample}_plasmid_prediction.tab",
        reference="reference_genome/{sample}_ref_genome.fasta",
        coverage="coverage/{sample}_estimation.txt",
        graph_contigs="coverage/{sample}_graph_contigs.tab",
        graph_repeats="coverage/{sample}_repeats_graph.tab",
        clean_prediction="coverage/{sample}_clean_prediction.tab",
        initialize_nodes="coverage/{sample}_initialize_nodes.tab",
        alignments="evaluation/{sample}_alignment_test.txt",
        solutions="walks/{sample}_solutions.csv",
        components="results/{sample}_bins.tab"
    output:
        metrics="evaluation/{sample}_metrics.tab"
    conda:
        "envs/r_packages.yaml"
    params:
        iterations = config["number_iterations"],
        classifier = config["classifier"],
        species = config["species"],
        name = config["name"]
    script:
        "scripts/gplas_evaluation.R"
